<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mar_hebrew');

/** MySQL database username */
define('DB_USER', 'USER');

/** MySQL database password */
define('DB_PASSWORD', 'PASS');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M4g3wMiZetGb.+eX%+-sLVxH5g`|!i60Nm~2T*J%--X.h$_0HaX#zzb}s>o9#Ud*');
define('SECURE_AUTH_KEY',  '5AO=PyMlB=Hd!>Od)$C-!8Ks)k*QtI<S &oHFz$0J~1>2ZX(!cv}zy}xk?@5]mGP');
define('LOGGED_IN_KEY',    'J[//VPdVm$Sv)l-?PP?cuR|l)y6.JD=4z?&JGPv[|<y9c#v47wNzHc+d1{wdkPGn');
define('NONCE_KEY',        'J05Y5V*Tk3Wj~FPR>;!(dNNq[*IA.+c[hAXA1o0:sa>V|m;ib`C6RfliLDJ/P4Y/');
define('AUTH_SALT',        'RNs%4e(h 9@0+UJw`7?2yKA|/|Gg}i2+`?`y?BK[I|<7+[IqC2a[kSV/3C,wY)yN');
define('SECURE_AUTH_SALT', '5jr^h<VeAtJxZ.4sQE5Im+s+qBZ cYsi9i+R9wy2]CV[6xL{6Z+-Q^h+ix~Ie,-f');
define('LOGGED_IN_SALT',   ' gbiZ4]Ydh7xJ#cqLnW LRpq?OPmMm9vng(uDW7!=boApoiK*39a<NS})gle2}P^');
define('NONCE_SALT',       'QV#?!|C:r]CY;+@`m66tH!L/RQLR3=$uGs!3zavINFL7KC<R%n*kWa9itRG@H9##');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
